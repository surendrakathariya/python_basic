import random
from hangman_words import word_list
from hangman_art import logo
from hangman_art import stages

print(logo)
# word_list = ["aardvark", "baboon", "camel"]
chosen_word = random.choice(word_list)
word_length = len(chosen_word)
end = False
lives = 6
# print(f'Solution is: ',chosen_word)

display = []

for _ in range(word_length):
    display += "_"

while not end:
    guess = input("Guess the word: ").lower()

    if guess in display:
        print(f"You have alresdy guessed {guess}")
    for position in range(word_length):
        letter = chosen_word[position]
        # print(f"current position:{position}\n current letter:{letter}\n Guessed letter:{guess}")
        if letter == guess:
            display[position] = letter
    if guess not in chosen_word:
        lives -= 1
        if lives == 0:
            end = True
            print("You lose")
    print(f" ".join(display))

    if "_" not in display:
        end = True
        print("You win")
    print(stages[lives])
