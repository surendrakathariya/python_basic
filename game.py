import random

user_input = input("Enter the input as either rock, paper, or scissor:")
option_list = ['Rock', 'Paper', 'Scissor']
comp_value = random.choice(option_list)

user_count = 0
comp_count = 0

if comp_value == 'Rock' and user_input == 'Paper' or \
        comp_value == 'Paper' and user_input == 'Scissor' or \
        comp_value == 'Scissor' and user_input == 'Rock':
    print("you won")
    user_count += 1

elif comp_value == 'Paper' and user_input == 'Rock' or \
        comp_value == 'Rock' and user_input == 'Scissor' or \
        comp_value == 'Scissor' and user_input == 'Paper':
    print("computer won")
    comp_count += 1

else:
    if comp_value == user_input:
        print("Tie")
        user_count += 1
        comp_count += 1
print(f'computer score:{comp_count}-Your score:{user_count}')
