import os

parent_path = "/home/suren/Desktop/miniproject/parent"

for i in os.listdir(parent_path):
    child_dir = os.path.join(parent_path, i)
    count_txt = 0
    count_png = 0
    count_jpeg = 0
    count_other = 0
    for j in os.listdir(child_dir):
        child_extention = j.split('.')
        if child_extention[-1] == ('txt'):
            count_txt += 1
        elif child_extention[-1] == ('jpeg'):
            count_jpeg += 1
        elif child_extention[-1] == ('png'):
            count_png += 1
        else:
            count_other += 1
    print(f'{i}: text:{count_txt} jpeg:{count_jpeg} png: {count_png} other:{count_other}')

#os.walk()
for root, dir, items in os.walk(parent_path):
   
    new_count = len(items)
    path_splitted = root.split('/')
    child_name = path_splitted[-1]

    if child_name in ("child2", "child1"):
        count_jpg = 0
        count_txt = 0
        count_png = 0
        count_jpeg = 0
        count_other = 0
        for i in items:
            x = i.split('.')
            ext = x[-1]
            if ext == 'jpg':
                count_jpg += 1
            elif ext == 'jpeg':
                count_jpeg += 1
            elif ext == 'png':
                count_png += 1
            elif ext == 'txt':
                count_txt += 1
            else:
                count_other +=1
        print(f'{child_name}: text:{count_txt} jpeg:{count_jpeg} png: {count_png} other:{count_other}')
