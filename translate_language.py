import random

bool_val = True
score = 0
translate_language = {"khana": "food", "khelnu": "play", "vhannu": "say"}

user_message = input("Do you want to play? y or n : ")
while bool_val:
    if user_message == 'y':
        print("Continue to play..")

        nepali_language = random.choice(list(translate_language.values()))
        print("Translate this word: ", nepali_language)

        user_input = input("Enter the translate word : ")
        for k, v in translate_language.items():
            if v == nepali_language:
                if user_input.lower() == k:
                    print("right")
                    score += 1
                else:
                    print("wrong")
                    score -= 1
                print("Your socre is: ", score)
    elif user_input == 'n':
        bool_val = False
