# Key as Value in dictionary from lists
name_list = ['Suren', 'Renuka', 'Mission']
address_list = ['Tikapur', 'Lalitpur', 'Kathmandu']
dict_exp = dict(zip(name_list, address_list))
print(dict_exp)


#Value index swapping
num1 = {1: 'One'}
num2 = {3: 'Three'}
swap_value1 = {}
swap_value2 = {}
for key1, value1 in num1.items():
    for key2, value2 in num2.items():
        swap_value1[key2] = value1
        swap_value2[key1] = value2

    print(swap_value1)
    print(swap_value2)
