from random import randint
dice_count = 0
snake_ladder_list = [
    # snake_list
    [10, 2],
    [8, 3],
    [9, 4],
    [4, 1],
    [12, 5],
    [15, 12],
    # ladder_list
    [10, 70],
    [21, 88],
    [7, 14],
    [60, 97]
]
val_set = set()
bool_value = True
# game_start = False
while bool_value:
    user_input = int(input("Enter 1 to play or 0 to exit:"))
    if user_input == 1:

        dice_roll = randint(1, 6)
        print("Dice value is:", dice_roll)
        print("\n")
        val_set.add(dice_roll)
        if 2 in val_set:
            print("You can play")
            dice_count += dice_roll
            print("count is", dice_count)

            for snake_count, i in enumerate(snake_ladder_list):
                for j, k in enumerate(i):
                    # print(j,k)
                    if j == 0:
                        if k == dice_count:
                            dice_count = i[1]
                            print("you've reached ", dice_count,
                                  "after either being eaten by snake or ladder")

            if dice_count >= 15:
                print("Victory")
                bool_value == False
            continue

    elif user_input == 0:
        bool_value = False
