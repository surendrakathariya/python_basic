import random
letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y',
           'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
numbers = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
symbols = ['!', '#', '$', '%', '&', '(', ')', '*', '+']

print("Welcome to the Password-Generator")

characters = list(letters + numbers + symbols)


def generate_random_password():
    length = int(input("Enter password length:"))

    letters_count = int(input("Enter the letters length: "))
    number_count = int(input("Enter the number length: "))
    symbols_count = int(input("Enter the symbols length: "))

    characters_count = letters_count + number_count + symbols_count
    print(characters_count)

    if characters_count > length:
        print("Password is greater than length")
        return
    password = []
    for i in range(letters_count):
        password.append(random.choice(letters))
    for i in range(number_count):
        password.append(random.choice(numbers))
    for i in range(symbols_count):
        password.append(random.choice(symbols))
    # print(password)
    if characters_count < length:
        random.shuffle(characters)
        for i in range(length - characters_count):
            password.append(random.choice(characters))
    random.shuffle(password)

    print("".join(password))


generate_random_password()
